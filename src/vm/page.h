#ifndef VM_PAGE_H
#define VM_PAGE_H

#include "kernel/synch.h"

#define FILE 0
#define SWAP 1

struct lock filesys_lock;

struct supp_page_entry {
  void *vaddr; /* virtual page number */
  uint8_t entry_type; /* FILE or SWAP*/

  bool loaded; /* Is this page loaded in memory? */
  bool writable; /* Is this page writable? */
  bool stack_page; //Is this a stack page

  /* FILE information */
  struct file *file;
  size_t offset;
  size_t read_bytes;
  size_t zero_bytes;

  /* SWAP information */
  size_t swap_index;

  struct hash_elem elem;
};

void supp_page_table_init(struct hash *spt);
void supp_page_table_destroy(struct hash *spt);
bool add_entry_to_supp_page_table(
  struct file *file, int32_t ofs, uint8_t *upage,
  uint32_t read_bytes, uint32_t zero_bytes,
  bool writable);
struct supp_page_entry *add_stack_entry_to_supp_page_table(void *vaddr);
struct supp_page_entry* get_spte(void *vaddr);
bool load_page_to_memory(struct supp_page_entry *spte);
bool load_page_from_file(struct supp_page_entry *spte);
bool load_page_from_swap(struct supp_page_entry *spte);
bool grow_stack(void *fault_addr);
#endif
