#ifndef VM_FRAME_H
#define VM_FRAME_H

#include "kernel/palloc.h"
#include "kernel/synch.h"
#include "vm/page.h"
#include <list.h>

struct list frame_table;
struct lock frame_table_lock;

struct frame_entry {
  void *frame; /* the physical frame */
  struct supp_page_entry *spte; /* supplementary page table entry for virtual page */
  struct thread *owner_thread; /* thread owning this page */
  struct list_elem elem;
};

/* initializes the frame table */
void frame_table_init (void);

/* allocates a physical frame */
void* frame_alloc (enum palloc_flags flags, struct supp_page_entry *spte);

/* frees a physical frame */
void frame_free (void *frame);

void *frame_evict(enum palloc_flags flags);

#endif
