#include <string.h>
#include <stdbool.h>
#include <stdio.h>
#include <hash.h>
#include "filesys/file.h"
#include "kernel/malloc.h"
#include "kernel/palloc.h"
#include "kernel/thread.h"
#include "kernel/vaddr.h"
#include "kernel/pagedir.h"
#include "kernel/process.h"
#include "vm/frame.h"
#include "vm/page.h"
#include "vm/swap.h"

#define PAGE_DEBUG 0
#define MAX_STACK_SIZE (1 << 23) // 256KB

/* Hashes the contents of hash_elem e and created a new supplementary page table entry */
static unsigned page_hash(const struct hash_elem *e, void *aux UNUSED) {
	const struct supp_page_entry *p = hash_entry(e, struct supp_page_entry, elem);
	return hash_int((int)p->vaddr);
}
/* Determines if the virtual address corresponding to hash_elem *a_ is less than that of hash_elem *b_ */
static bool page_less(const struct hash_elem *a_, const struct hash_elem *b_, void *aux UNUSED) {
	const struct supp_page_entry *a = hash_entry(a_, struct supp_page_entry, elem);
	const struct supp_page_entry *b = hash_entry(b_, struct supp_page_entry, elem);

	return (a->vaddr < b->vaddr);
}

/* Deallocate memory used by hash element e */
static void page_clear(struct hash_elem *e, void *aux UNUSED) {
	struct supp_page_entry *spte = hash_entry(e, struct supp_page_entry, elem);
        if (spte->loaded) {
            frame_free(pagedir_get_page(thread_current()->pagedir, spte->vaddr));
            pagedir_clear_page(thread_current()->pagedir, spte->vaddr);
         }
         free(spte);
}

/* Initialize the supplementary page table */
void supp_page_table_init(struct hash *spt) {
	hash_init(spt, page_hash, page_less, NULL);
}

/* Destroy the supplementary page table */
void supp_page_table_destroy(struct hash *spt) {
	hash_destroy(spt, page_clear);
}

/* Add an entry to the supplementary page table */
bool add_entry_to_supp_page_table(
  struct file *file, int32_t ofs, uint8_t *upage,
  uint32_t read_bytes, uint32_t zero_bytes,
  bool writable) {
	struct supp_page_entry *spte = malloc(sizeof(struct supp_page_entry));
  if(!spte)
    return false;
	spte->vaddr = (void *)upage;
	spte->offset = (size_t)ofs;
	spte->file = file;
	spte->read_bytes = read_bytes;
	spte->zero_bytes = zero_bytes;
	spte->writable = writable;
  spte->loaded = false;
  spte->entry_type = FILE;
  spte->stack_page = ((spte->vaddr < PHYS_BASE) && (spte->vaddr >= PHYS_BASE-PGSIZE));
  spte->swap_index = -1; 
	return (hash_insert(&thread_current()->spt,&spte->elem) == NULL);  
}

/* Add a stack entry to the supplementary page table */
struct supp_page_entry *add_stack_entry_to_supp_page_table(void *vaddr){
  struct supp_page_entry *spte = malloc(sizeof(struct supp_page_entry));
  if(!spte)
    return NULL;
  spte->vaddr = vaddr;
  spte->loaded = true;
  spte->writable = true;
  spte->stack_page = true;
  if (hash_insert(&thread_current()->spt,&spte->elem) == NULL)
    return spte;
  else
    return NULL;
}

/* Get a supplementary page table entry that corresponds to a given virtual address */
struct supp_page_entry* get_spte(void *vaddr){
  struct supp_page_entry spe;
  spe.vaddr = pg_round_down(vaddr);
  struct hash_elem *e;
  e = hash_find(&thread_current()->spt,&spe.elem);
  if(!e)
    return NULL;
  return hash_entry(e,struct supp_page_entry,elem);
}

/* Load a page to memory */
bool load_page_to_memory(struct supp_page_entry *spte){
  bool success = false;
  if(spte->entry_type == FILE)
    success = load_page_from_file(spte);
  else if(spte->entry_type == SWAP)
    success = load_page_from_swap(spte);
  return success;
}

/* Load a page from a file to memory */
bool load_page_from_file(struct supp_page_entry *spte){
  enum palloc_flags flags = PAL_USER;
  if(spte->zero_bytes == PGSIZE)
    flags = flags | PAL_ZERO;
  void *frame = frame_alloc(flags,spte);
  if(!frame)
    return false;
  if(spte->read_bytes > 0){
    lock_acquire(&filesys_lock);
    size_t n_bytes = file_read_at(spte->file,frame,spte->read_bytes,spte->offset); 
    if(n_bytes != spte->read_bytes){
      lock_release(&filesys_lock);
      frame_free(frame);
      return false;
    }
    lock_release(&filesys_lock);
  }
  memset (frame + spte->read_bytes, 0, spte->zero_bytes);
  /* Add the page to the process's address space. */
  if (!install_page (spte->vaddr, frame, spte->writable)) 
  {
    frame_free(frame);
    return false; 
  }
  spte->loaded = true;
  return true; 
}

/* Load a page from swap space into memory */
bool load_page_from_swap(struct supp_page_entry *spte){
  enum palloc_flags flags = PAL_USER;
  void *frame = frame_alloc(flags,spte);
  if(!frame)
    return false;
  //code to get from swap, use block and index var from spte struct
  read_from_swap_disk(spte->swap_index, frame);  
  /* Add the page to the process's address space. */
  if (!install_page (spte->vaddr, frame, spte->writable)) 
    {
      frame_free(frame);
      return false; 
    }
  spte->loaded = true;
  return true; 
}

/* Creates a new supplementary page table entry and allocates a corresponding frame */
bool grow_stack(void *fault_addr){
  if ((PHYS_BASE - pg_round_down(fault_addr)) > MAX_STACK_SIZE)
    return false;
  struct supp_page_entry *spte = malloc(sizeof(struct supp_page_entry));
  if(!spte)
    return false;
  spte->vaddr = pg_round_down(fault_addr);
  spte->loaded = true;
  spte->writable = true;
  spte->stack_page = true;
  spte->entry_type = SWAP;
  enum palloc_flags flags = PAL_USER;
  void *frame = frame_alloc(flags,spte);
  if(!frame)
    return false;
      /* Add the page to the process's address space. */
      if (!install_page (spte->vaddr, frame, spte->writable))
        {
          frame_free(frame);
          return false;
        }
  return (hash_insert(&thread_current()->spt,&spte->elem) == NULL);
}


