#ifndef VM_SWAP_H
#define VM_SWAP_H

#include "devices/block.h"
#include "kernel/synch.h"
#include "kernel/vaddr.h"
#include <bitmap.h>

#define SWAP_FREE 0
#define SWAP_INUSE 1
#define SECTORS_PER_PAGE (PGSIZE/BLOCK_SECTOR_SIZE)

/* lock for critical sections while accessing swap disk */
struct lock swap_lock;

/* bitmap data structure for recording which swap slot is in use and which is free */
struct bitmap *swap_bitmap;

/* the swap disk */
struct block *swap_disk;

/* initialize swap disk and swap bitmap and lock */
void swap_init(void);

/* Write frame from memory to swap disk - find an open slot and write to that slot;
 * return the slot number; If no available swap slot, PANIC */
size_t write_to_swap_disk(void *frame);

/* Read from swap disk from slot INDEX and write to memory frame FRAME */
void read_from_swap_disk(size_t index, void* frame);

#endif 
