#include "kernel/palloc.h"
#include "kernel/malloc.h"
#include "kernel/thread.h"
#include "kernel/synch.h"
#include "kernel/pagedir.h"
#include <stdio.h>
#include "vm/page.h"
#include "vm/frame.h"
#include "vm/swap.h"

#define FRAME_DEBUG 0

/* initializes the frame table */
void frame_table_init (void){
  list_init(&frame_table);
  lock_init(&frame_table_lock);
  lock_init(&filesys_lock);
}

/* allocates a physical frame */
void* frame_alloc (enum palloc_flags flags, struct supp_page_entry *spte){
  if (FRAME_DEBUG) printf("INSIDE FRAME ALLOC****\n");
  if(!(flags & PAL_USER))
    return NULL;
  //obtain a frame
  void *frame = palloc_get_page(flags);  
  //if no frame was available you must evict an existing page in the frame table
  if(frame == NULL){
    frame = frame_evict(flags);   
  }
  else{
    struct frame_entry *entry = malloc(sizeof(struct frame_entry)); 
    entry->frame = frame;
    entry->spte = spte;
    entry->owner_thread = thread_current();
    //push the frame to the list in the critical section
    lock_acquire (&frame_table_lock);
    list_push_back(&frame_table, &(entry->elem));
    lock_release (&frame_table_lock);
  }
  return frame;
}

/* frees a physical frame */
void frame_free (void *frame){
  struct list_elem *e;
  lock_acquire (&frame_table_lock); //maybe this can go inside if statement of for loop
  for (e = list_begin (&frame_table); e != list_end (&frame_table); e = list_next (e)){
    struct frame_entry *entry = list_entry(e, struct frame_entry, elem);
    if(entry->frame == frame){
      list_remove(e);
      free(entry);
      palloc_free_page(frame);
      break; //found the frame so no need to keep searching
    }
  }
  lock_release (&frame_table_lock);
}

void *frame_evict(enum palloc_flags flags){
  struct list_elem *e = list_begin(&frame_table); //get the first element of the list
  struct frame_entry *fte = list_entry(e,struct frame_entry,elem); 
  struct thread *t = fte->owner_thread; //get the thread that owns the frame
  size_t index = write_to_swap_disk(fte->frame); //write the frame to the swap disk
  fte->spte->swap_index = index; //set the location of the frame in the swap disk
  fte->spte->entry_type = SWAP; //designate this page as a SWAP page
  fte->spte->loaded = false; //page is no longer loaded in memory
  list_remove(&fte->elem); //remove the page from the frame list (phsyical memory) 
  pagedir_clear_page(t->pagedir,fte->spte->vaddr); //clears the low-level page table
  /* free the frame from physical memory */
  palloc_free_page(fte->frame); 
  free(fte);
  return palloc_get_page(flags);
}

