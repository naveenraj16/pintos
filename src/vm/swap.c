#include "vm/swap.h"

#define SWAP_DEBUG 0

/* initialize swap disk and swap bitmap and lock */
void swap_init(void){
  swap_disk = block_get_role(BLOCK_SWAP);
  if(!swap_disk)
    return;
  swap_bitmap = bitmap_create(block_size(swap_disk)/SECTORS_PER_PAGE); 
  if(!swap_bitmap)
    return;
  bitmap_set_all(swap_bitmap,SWAP_FREE);
  lock_init(&swap_lock);
}

/* Write frame from memory to swap disk - find an open slot and write to that slot;
 * return the slot number; If no available swap slot, PANIC */
size_t write_to_swap_disk(void *frame){
  lock_acquire(&swap_lock);
  size_t index = bitmap_scan_and_flip(swap_bitmap,0,1,SWAP_FREE);
  if(index == BITMAP_ERROR)
    PANIC("SWAP SLOT FULL");
  size_t i;
  for(i = 0; i < SECTORS_PER_PAGE; i++){
    block_write(swap_disk, index*SECTORS_PER_PAGE + i, frame + i*BLOCK_SECTOR_SIZE);
  }
  lock_release(&swap_lock);
  return index;
}

/* Read from swap disk from slot INDEX and write to memory frame FRAME */
void read_from_swap_disk(size_t index, void* frame){
  lock_acquire(&swap_lock);
  if(bitmap_test(swap_bitmap,index) == SWAP_FREE)
    PANIC("Cannot read from a free swap slot");
  size_t i;
  for(i = 0; i < SECTORS_PER_PAGE; i++){
    block_read(swap_disk, index*SECTORS_PER_PAGE + i, frame + i*BLOCK_SECTOR_SIZE);
  }
  bitmap_flip(swap_bitmap,index);
  lock_release(&swap_lock);
}

